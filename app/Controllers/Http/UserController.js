'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const { validate } = use('Validator')
const Query = use('Query')
const User = use("App/Models/User")

/**
 * Resourceful controller for interacting with users
 */
class UserController {
  constructor() {
    this.validate = {
      rules: {
        name: 'required|unique:users,name',
        email: 'required|email|unique:users,email',
        password: 'required|min:6',
      },
      messages: {
        'name.unique': 'O nome deve ser único.',
        'name.required': 'O nome deve ser informado.',
        'email.email': 'O email é inválido.',
        'email.required': 'O email deve ser informado.',
        'email.unique': 'O email deve ser único.',
        'password.required': 'A senha deve ser informada.',
        'password.min': 'A senha deve possuir no mínimo 6 dígitos.'
      }
    }
  }
  /**
   * Show a list of all users.
   * GET users
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    const page = request.get().page || 1
    const limit = request.get().limit || 10000

    const { sort = 'id', direction = 'asc' } = request.all()
    const query = new Query(request, { order: 'id' })

    return await User.query()
      .where(query.search([
        'name',
        'email'
      ]))
      .orderBy(sort, direction)
      .paginate(page, limit)
  }

  /**
   * Create/save a new client.
   * POST users
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    const data = request.only(["name", "email", "password"])

    const validation = await validate(data, this.validate.rules, this.validate.messages)

    if (validation.fails()) {
      return response.status(400).send({
        errors: validation.messages()
      })
    }

    return User.create(data)
  }

  /**
   * Display a single client.
   * GET users/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
    return await User.findOrFail(params.id)
  }

  /**
   * Update client details.
   * PUT or PATCH users/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const validation = {
      rules: {
        email: `email|unique:users,email,id,${params.id}`,
        name: `unique:users,name,id,${params.id}`,
      },
      messages: {
        'email.email': 'Informe um email válido.',
        'email.unique': 'Este email já existe.'
      }
    }
    const data = request.only(["email", "password", "name"])

    const validator = await validate(data, validation.rules, validation.messages)

    if (validator.fails()) {
      return response.status(400).send({
        errors: validator.messages()
      })
    }
    const user = await User.findOrFail(params.id)

    user.merge(data)
    user.save()

    return user
  }

  /**
   * Delete a client with id.
   * DELETE users/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    const user = await User.find(params.id)
    await user.delete()

    return user
  }
}

module.exports = UserController
