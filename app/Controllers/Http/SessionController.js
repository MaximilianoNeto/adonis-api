'use strict'

class SessionController {
  async store({ request, auth }) {
    const { refresh_token, email, password } = request.all()

    if (refresh_token) {
      return await auth
        .generateForRefreshToken(refresh_token);
    }

    const token = await auth.withRefreshToken().attempt(email, password)

    return token
  }
}

module.exports = SessionController
