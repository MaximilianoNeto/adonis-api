'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const { validate } = use('Validator')
const Query = use('Query')
const City = use("App/Models/City")

/**
 * Resourceful controller for interacting with cities
 */
class CityController {
  constructor() {
    this.validate = {
      rules: {
        name: 'required|unique:cities,name',
        state: 'required'

      },
      messages: {
        'name.unique': 'O nome deve ser único.',
        'name.required': 'O nome deve ser informado.',
        'state.required': 'O estado deve ser informado.'
      }
    }
  }
  /**
   * Show a list of all cities.
   * GET cities
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    const page = request.get().page || 1
    const limit = request.get().limit || 10000

    const { sort = 'id', direction = 'asc' } = request.all()
    const query = new Query(request, { order: 'id' })

    return await City.query()
      .where(query.search([
        'name',
        'state'
      ]))
      .orderBy(sort, direction)
      .paginate(page, limit)
  }

  /**
   * Create/save a new city.
   * POST cities
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    const data = request.only(["name", "state"])

    const validation = await validate(data, this.validate.rules, this.validate.messages)

    if (validation.fails()) {
      return response.status(400).send({
        errors: validation.messages()
      })
    }

    return City.create(data)
  }

  /**
   * Display a single city.
   * GET cities/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
    return await City.findOrFail(params.id)
  }

  /**
   * Update city details.
   * PUT or PATCH cities/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const validation = {
      rules: {
        name: `unique:cities,name,id,${params.id}`,
      },
      messages: {
        'name.unique': 'O nome deve ser único',
      }
    }

    const data = request.only(["name", "state"])

    const validator = await validate(data, validation.rules, validation.messages)

    if (validator.fails()) {
      return response.status(400).send({
        errors: validator.messages()
      })
    }

    const city = await City.findOrFail(params.id)

    await city.merge(data)
    await city.save()

    return city
  }

  /**
   * Delete a city with id.
   * DELETE cities/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    const city = await City.find(params.id)
    await city.delete()

    return city
  }
}

module.exports = CityController
