'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const { validate } = use('Validator')
const Query = use('Query')
const Client = use("App/Models/Client")
const moment = require('moment')

/**
 * Resourceful controller for interacting with clients
 */
class ClientController {
  constructor() {
    this.validate = {
      rules: {
        name: 'required|unique:clients,name',
        sex: 'required',
        date_of_birth: 'required',
        city_id: 'required',

      },
      messages: {
        'name.unique': 'O nome deve ser único.',
        'name.required': 'O nome deve ser informado.',
        'sex.required': 'O sexo deve ser informado.',
        'date_of_birth.required': 'A data de nascimento deve ser informada.',
        'city_id.required': 'A cidade deve ser informada.'
      }
    }
  }
  /**
   * Show a list of all clients.
   * GET clients
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    const page = request.get().page || 1
    const limit = request.get().limit || 10000

    const { sort = 'id', direction = 'asc' } = request.all()
    const query = new Query(request, { order: 'id' })

    return await Client.query()
      .where(query.search({
        'id': Query.INT,
        'name': Query.STRING
      }))
      .with('city')
      .orderBy(sort, direction)
      .paginate(page, limit)
  }

  /**
   * Create/save a new client.
   * POST clients
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    const data = request.only(["name", "sex", "date_of_birth", "city_id"])

    const validation = await validate(data, this.validate.rules, this.validate.messages)

    if (validation.fails()) {
      return response.status(400).send({
        errors: validation.messages()
      })
    }

    const age = moment().diff(data.date_of_birth, 'years');

    const client_data = {
      ...data,
      age: age
    }

    return Client.create(client_data)
  }

  /**
   * Display a single client.
   * GET clients/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
    return await Client.query().where('id', params.id).with('city').first()
  }

  /**
   * Update client details.
   * PUT or PATCH clients/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const validation = {
      rules: {
        email: `email|unique:users,email,id,${params.id}`,
        name: `unique:users,name,id,${params.id}`,
      },
      messages: {
        'email.email': 'Informe um email válido.',
        'email.unique': 'Este email já existe.'
      }
    }
    const data = request.only(["email", "password", "name"])

    const validator = await validate(data, validation.rules, validation.messages)

    if (validator.fails()) {
      return response.status(400).send({
        errors: validator.messages()
      })
    }

    const client = await Client.findOrFail(params.id)

    const age = moment().diff(data.date_of_birth, 'years');

    const client_data = {
      ...data,
      age: age
    }

    await client.merge(client_data)
    await client.save()

    return client
  }

  /**
   * Delete a client with id.
   * DELETE clients/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    const client = await Client.find(params.id)
    await client.delete()

    return client
  }
}

module.exports = ClientController
