# Adonis API

Esta é um api para cadastro de clientes, cidades que usa como autenticação o JWT.

## Configuração

Use este comando para instalar as dependencias.

```bash
npm i
```

### Migrações

Rode este comando para criar as migrações e configurar o banco.

```js
adonis migration:run
```

### Seed

Rode este comando para criar o primeiro usuário da api.

```js
adonis seed
```
