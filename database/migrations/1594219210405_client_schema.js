'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ClientSchema extends Schema {
  up () {
    this.create('clients', (table) => {
      table.increments()
      table.string('name').notNullable()
      table.string('sex').notNullable()
      table.integer('age').notNullable()
      table.timestamp('date_of_birth').notNullable()
      table.integer('city_id').unsigned().index()
      table.foreign('city_id').references('id').on('cities')
      table.timestamps()
    })
  }

  down () {
    this.drop('clients')
  }
}

module.exports = ClientSchema
