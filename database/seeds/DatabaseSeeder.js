'use strict'

/*
|--------------------------------------------------------------------------
| DatabaseSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const User = use("App/Models/User")

class DatabaseSeeder {
  async run () {
    await User.findOrCreate(
      { email: 'admin@email.com' },
      {
        name: 'Administrador',
        email: 'admin@email.com',
        password: 'secret'
      }
    )
  }
}

module.exports = DatabaseSeeder
